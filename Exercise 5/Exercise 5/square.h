#pragma once

#include "shape.h"

class square : public shape
{
public:
	square(int length);
	~square();

	int getLength();
	void setLength(int Value);
	int getArea();

private:
	int sLength;
	int sArea;
};

