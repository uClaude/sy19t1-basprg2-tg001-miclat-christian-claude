#pragma once
#include <string>

using namespace std;

class shape
{	
public:
	shape();
	~shape();

	string getName();
	int getSides();

	void setName(string name);
	void setSides(int sides);
	virtual int getArea();

private:
	
	string sName;
	int sSides;
};

