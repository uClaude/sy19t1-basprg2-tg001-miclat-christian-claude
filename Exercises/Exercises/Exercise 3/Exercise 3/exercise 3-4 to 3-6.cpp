#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;

//Create a function which creates a random item based on the loot table.
//Return a pointer of the item back to the caller.
void itemList()
{
	struct Item
	{
		string mithril_Ore;
		string sharp_Talon;
		string thick_Leather;
		string jellopy;
		string cursed_Stone;
		int mithril = 100;
		int talon = 50;
		int leather = 25;
		int jelly = 5;
		int badStone = 0;
	};
}

//Create an enterDungeon function.
//Deduct the dungeon fee(25)
//Continue looting until a Cursed Stone is looted
//Gold is not immediately added to the player until the player chooses to exit out of the dungeon.
//If player loots any item aside from the Cursed Stone, ask the player if he wants to continue looting.
//Choosing yes will increase the bonus multiplier by 1.
//Choosing no will end the dungeon instance.All gold looted from the instance will be added to the player�s gold.


int main()
{
	srand(time(NULL));
	int gold = 50;
	bool game = true;

	while (game)
	{
		if (gold >= 500 || gold < 25)
		{
			cout << "= = Game Over = =" << endl;
			system("pause");
		}
		system("pause");
	}
	return 0;
}