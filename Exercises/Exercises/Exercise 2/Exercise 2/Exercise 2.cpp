#include <iostream>
#include<time.h>
#include<string>

using namespace std;


void bet(int& gold, int& bet ) //ex 2 - 1
{
	gold -= bet;
}

int diceRoll(int die1, int die2, int& diceSum) //ex 2 - 2
{
	die1 = rand() % 6 + 1;
	die2 = rand() % 6 + 1;
	
	return diceSum = die1 + die2;
}

int aidiceRoll(int AIdie1, int AIdie2, int& aidiceSum) //ex 2 - 2
{
	AIdie1 = rand() % 6 + 1;
	AIdie2 = rand() % 6 + 1;

	return aidiceSum = AIdie1 + AIdie2;
}

void diceResult(int& diceSum, int& aidiceSum, int& gold, int& bet) //ex 2 - 3
{
	if (diceSum == 2)
	{
		gold += (bet*3);
		cout << "Snake Eyes!" << endl;
	}
	else if (aidiceSum == 2)
	{
		cout << "Enemy Snake Eyes!" << endl;
	}
	else if (diceSum > aidiceSum)
	{
		gold += bet;
		cout << "Player Wins" << endl;
	}
	else if (diceSum == aidiceSum)
	{
		gold += bet;
		cout << "Tie!" << endl;
	}
	else cout << "AI Wins" << endl;
}

void playRound(int die1, int die2, int& diceSum, int& aidiceSum, int& gold, int& pbet) //ex 2- 4
{
	cout << "Player roll" << endl << endl;
	cout << "Roll: " << diceRoll(die1, die2, diceSum) << endl;

	cout << "==========" << endl;

	cout << "AI roll" << endl << endl;
	cout << "Roll: " << aidiceRoll(die1, die2, aidiceSum) << endl;

	diceResult(diceSum, aidiceSum, gold, pbet);

	cout << "current Gold: " << gold << endl;
}

int main()
{
	srand(time(NULL));
	int gold = 1000;
	int pbet;
	int die1 = 6;
	int die2 = 6;
	int AIdie1 = 6;
	int AIdie2 = 6;
	int diceSum = 0;
	int aidiceSum = 0;

	//Create a function that asks the player for his bet and deducts the user�s gold immediately.
	//This function must accept a reference.

	while (gold > 0)
	{
		cout << "How much gold to bet?: ";
		cin >> pbet;

		bet(gold, pbet);

		//cout << "Current Bet: " << gold << endl;
		
		system("pause");
		system("cls");

		playRound(die1, die2, diceSum, aidiceSum, gold, pbet);

		system("pause");
		system("cls");
	}
	cout << "you lost" << endl;
	system("pause");
	return 0;
}