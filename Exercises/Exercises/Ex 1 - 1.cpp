#include <iostream>
#include <string>
#include <time.h>

using namespace std;

int factorial(int terms)
{
	int fact = 1;
	
	for (int i = 1; i <= terms; i++)
	{
		fact = fact * i;
	}
	cout << fact << endl;
	return fact;
}


int main()
{
	int terms;

	cout << "Give a number to factorial : ";
	cin >> terms;

	cout << "Factorial: ";
	factorial(terms);


	system("pause");
	return 0;
}