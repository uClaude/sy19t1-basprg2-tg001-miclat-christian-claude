#include <iostream>
#include <string>
#include <time.h>

using namespace std;

void itemList()
{
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	int num = 7;

	for (int i = 0; i <= num; i++)
	{
		cout << items[i] << endl;
	}
}

int main()
{
	cout << "=== ITEM LIST ===" << endl;

	itemList();

	system("pause");
	return 0;
}