#pragma once
#include <iostream>

using namespace std;

class character;

class items
{
public:
	items(string name);
	~items();

	string getName();	
	
	virtual void use(character* item);

private:
	string mName;
};

