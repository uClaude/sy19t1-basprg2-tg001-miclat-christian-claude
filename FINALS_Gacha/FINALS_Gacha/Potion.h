#pragma once
#include <string>
#include <iostream>
#include "items.h"

using namespace std;

class character;

class Potion : public items
{
public:
	Potion(string name, int potionEffect);
	~Potion();

	void use(character* item) override;

private:
	int pPotionEffect;
};

