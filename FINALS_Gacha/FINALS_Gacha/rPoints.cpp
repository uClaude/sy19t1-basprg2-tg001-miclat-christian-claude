#include "rPoints.h"


rPoints::rPoints(string name, int pointEffect): items(name)
{
	rPointsEffect = pointEffect;
}

rPoints::~rPoints()
{
}

void rPoints::use(character* item) 
{
	item->rarePoint(rPointsEffect);
	
}
