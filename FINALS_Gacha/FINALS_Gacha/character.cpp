#include "character.h"
#include <iostream>
#include <string>

using namespace std;

character::character(string name, int rarity, int crystals, int health)
{
	pName = name;
	pRp = rarity;
	pCrystal = crystals;
	pHp = health;

}

character::~character()
{
}

int character::getRarepoints()
{
	return pRp;
}

int character::getCrystal()
{
	return pCrystal;
}

int character::getHp()
{
	return pHp;
}

string character::getName()
{
	return pName;
}

void character::heal(int value)
{
	pHp += value;
}

void character::damage(int value)
{
	pHp -= value;
}

void character::addCrystal(int value)
{
	pCrystal += value;
}

void character::lossCrystal(int value)
{
	pCrystal -= value;
}

void character::rarePoint(int value)
{
	pRp += value;
}


