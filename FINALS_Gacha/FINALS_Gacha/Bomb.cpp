#include "Bomb.h"
#include "character.h"

Bomb::Bomb(string name, int bombEffect) : items(name)
{
	pBombEffect = bombEffect;
}

Bomb::~Bomb()
{
}

void Bomb::use(character* item)
{
	item->damage(pBombEffect);
}
