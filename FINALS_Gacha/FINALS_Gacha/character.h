#pragma once
#include <string>
#include "items.h"


using namespace std;

class items;

class character
{	
public:
	character(string name, int pRp, int pCrystal, int pHp);
	~character();

	int getRarepoints();
	int getCrystal();
	int getHp();
	string getName();

	void heal(int value);
	void damage(int value);
	void addCrystal(int value);
	void lossCrystal(int value);
	void rarePoint(int value);


private:
	
	string pName;
	int pRp;
	int pCrystal;
	int pHp;

};

