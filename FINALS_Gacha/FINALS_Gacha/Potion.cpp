#include "Potion.h"
#include "character.h"

Potion::Potion(string name, int potionEffect): items(name)
{
	pPotionEffect = potionEffect;
}

Potion::~Potion()
{
}

void Potion::use(character* item)
{
	item->heal(pPotionEffect);
}

