#pragma once
#include <iostream>
#include <string>
#include "items.h"
#include "character.h"
using namespace std;

class character;

class rPoints : public items
{
public:
	rPoints(string name, int pointEffect);
	~rPoints();

	virtual void use(character* item);

private:
	int rPointsEffect;
};

