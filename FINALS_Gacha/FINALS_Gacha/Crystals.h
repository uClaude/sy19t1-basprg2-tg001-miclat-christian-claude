#pragma once
#include <string>
#include <iostream>
#include "items.h"


using namespace std;

class character;

class Crystal : public items
{
public:
	Crystal(string name, int crystalEffect);
	~Crystal();

	void use(character* item) override;

private:
	int pCrystalEffect;
};

