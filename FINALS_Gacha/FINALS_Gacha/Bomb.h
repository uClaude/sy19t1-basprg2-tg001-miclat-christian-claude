#pragma once
#include <string>
#include <iostream>
#include "items.h"

using namespace std;

class character;

class Bomb : public items
{
public:
	Bomb(string name, int bombEffect);
	~Bomb();

	void use(character* item) override;

private:
	int pBombEffect;
};

