#include "Crystals.h"
#include "character.h"

Crystal::Crystal(string name, int crystalEffect) : items(name)
{
	pCrystalEffect = crystalEffect;
}

Crystal::~Crystal()
{
}

void Crystal::use(character* item)
{
	item->addCrystal(pCrystalEffect);
}
