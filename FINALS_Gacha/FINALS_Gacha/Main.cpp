#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <conio.h>
#include "Bomb.h"
#include "character.h"
#include "Crystals.h"
#include "items.h"
#include "Potion.h"
#include "rPoints.h"

using namespace std;

items* randomItem(vector<items*>itemList, character* Character, int& ssr, int& sr, int& health, int& crystal, int& r, int& bomb)
{
	int randPull = rand() % 100 + 1;
	items* pull = new items("");

	if (randPull == 1)
	{
		pull = itemList[5];
		pull->use(Character);
		cout << "You've Pulled a/n >> " << pull->getName() << " <<" << endl;
		cout << "You received 50 Rarity Points!!" << endl;
		ssr++;
	}
	else if (randPull > 1 && randPull <= 10)
	{
		pull = itemList[4];
		pull->use(Character);
		cout << "You've Pulled a/n >> " << pull->getName() << " <<" << endl;
		cout << "You received 10 Rarity Points!!" << endl;
		sr++;
	}
	else if (randPull > 10 && randPull <= 40)
	{
		int flip = rand() % 2 + 1;
		if (flip == 1)
		{
			pull = itemList[1];
			pull->use(Character);
			cout << "You've Pulled a/n >> " << pull->getName() << " <<" << endl;
			cout << "You received 30 Health Points!!" << endl;
			health++;
		}
		else if (flip == 2)
		{
			pull = itemList[0];
			pull->use(Character);
			cout << "You've Pulled a/n >> " << pull->getName() << " <<" << endl;
			cout << "You received 15 Crystals!! **Free 2 more pulls**" << endl;
			crystal++;
		}
	}
	else if (randPull > 40 && randPull <= 80)
	{
		pull = itemList[3];
		pull->use(Character);
		cout << "You've Pulled a/n >> " << pull->getName() << " <<" << endl;
		cout << "You received 1 Rarity Point!!" << endl;
		r++;
	}
	else if (randPull > 80 && randPull <= 100)
	{
		pull = itemList[2];
		pull->use(Character);
		cout << "You've Pulled a/n >> " << pull->getName() << " <<" << endl;
		cout << "You lost 25 Hp!!" << endl;
		bomb++;
	}
	return pull;
}

void collateItems(int& ssr, int& sr, int& health, int& crystal, int& r, int& bomb)
{
	cout << "SSR: [" << ssr << "]" << endl;
	cout << "SR: [" << sr << "]" << endl;
	cout << "R: [" << r << "]" << endl;
	cout << "HP Pots: [" << health << "]" << endl;
	cout << "Crystal Pulls: [" << crystal << "]" << endl;
	cout << "Bomb Pulls: [" << bomb << "]" << endl;
}

int main()
{
	srand(time(NULL));
		
	int ssrCount = 0;
	int srCount = 0;
	int rCount = 0;
	int bombCount = 0;
	int healthCount = 0;
	int crystalCount = 0;

	character* Character = new character("HP",0, 100, 100);
	Crystal* crystals = new Crystal("Crystals", 15);
	Potion* potion = new Potion("Health Potion", 30);
	Bomb* bomb = new Bomb("Bomb", 25);	
	rPoints* rarity = new rPoints("R", 1);
	rPoints* rarity1 = new rPoints("SR", 10);
	rPoints* rarity2 = new rPoints("SSR", 50);

	vector<items*>itemList;
	itemList.push_back(crystals); // index 0
	itemList.push_back(potion); // index 1
	itemList.push_back(bomb); // index 2 
	itemList.push_back(rarity); // index 3
	itemList.push_back(rarity1); //index 4
	itemList.push_back(rarity2); //index 5

	cout << "!! ITEMS FOR GRAB !!" << endl;
	for (int i = 0; i < itemList.size(); i++)
	{
		items* showItems = itemList[i];
		cout << showItems->getName() << endl;
	}

	system("pause");
	system("cls");

	vector<items*>itemSummary;

	int turn = 0;
	while (Character->getRarepoints() <= 100 || Character->getCrystal() >= 0 || Character->getHp() >= 0)
	{
		cout << "=====================" << endl;
		cout << Character->getName() << "          : " << Character->getHp() << "    =" << endl;
		cout << crystals->getName() << "     : " << Character->getCrystal() << "   =" << endl;
		cout << "Rare Points" << " : " << Character->getRarepoints() << "    =" << endl;
		cout << "Pulls  : [" << turn << "]       =" << endl;
		cout << "=====================" << endl << endl;
		itemSummary.push_back(randomItem(itemList, Character, ssrCount, srCount, healthCount, crystalCount, rCount ,bombCount));
		system("pause");
		system("cls");

		turn++;
		Character->lossCrystal(5);

		if (Character->getRarepoints() >= 100) break;
		else if (Character->getCrystal() <= 0) break;
		else if (Character->getHp() <= 0) break;
	}
	cout << "History of Pulls:";
	for (int i = 0; i < itemSummary.size(); i++)
	{
		items* showItems = itemSummary[i];
		cout << showItems->getName() << " ";
	} cout << endl;
	cout << "You've pulled: " << endl;
	collateItems(ssrCount, srCount, healthCount, crystalCount, rCount, bombCount);






	system("pause");
	return 0;
}