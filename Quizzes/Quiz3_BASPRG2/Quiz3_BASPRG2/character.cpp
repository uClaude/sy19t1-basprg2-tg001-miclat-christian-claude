#include "character.h"
#include <iostream>
using namespace std;

character::character(string name, int hp, int pow, int vit, int agi, int dex)
{
	pName = name;
	pHp = hp;
	pPow = pow;
	pVit = vit;
	pAgi = agi;
	pDex = dex;
}

character::~character()
{
}

string character::getName()
{
	return pName;
}

int character::getHp()
{
	return pHp;
}

void character::damageTaken(int dmg)
{
	pHp -= dmg;
}

void character::battleHeal(int hp)
{
	pHp += (hp * 0.30);
	cout << pHp;
}

int character::getPow()
{
	return pPow;
}

int character::getVit()
{
	return pVit;
}

int character::getAgi()
{
	return pAgi;
}

int character::getDex()
{
	return pDex;
}
