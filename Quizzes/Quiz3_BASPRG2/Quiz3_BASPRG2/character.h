#pragma once
#include <string>

using namespace std;

class character
{
public:
	character(string name, int hp, int pow, int vit, int agi, int dex);
	~character();

	string getName();
	int getHp();
	void damageTaken(int dmg);
	void battleHeal(int hp);
	
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

private:

	string pName;
	int pHp;
	int pPow;
	int pVit;
	int pAgi;
	int pDex;


};

