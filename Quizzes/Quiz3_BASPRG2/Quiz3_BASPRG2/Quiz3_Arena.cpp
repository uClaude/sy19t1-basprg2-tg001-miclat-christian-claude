#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include "character.h"

using namespace std;

void randClass(int pClass)
{
	int rClass;
	rClass = rand() % 3 + 1;
	if (rClass == 1)
	{
		cout << "Warrior";
	}
	else if (rClass == 2)
	{
		cout << "Assassin";
	}
	else if (rClass == 3)
	{
		cout << "Wizard";
	}
	
}

int battleIncident(int Vit, int Pow)
{
	Pow -= Vit;
	return Pow;
}

void battle(character* player, character* enemy)
{
	int i;
	
	if (player->getAgi() > enemy->getAgi())
	{
		cout << "Player goes first" << endl << endl;
		i = battleIncident(rand() % enemy->getVit(),rand() % player->getPow());
		cout << "Player dealt " << i << " to the enemy " << endl;
		if (i < 0) (i * -1);
		else if (i == 0) i = 1;
		cout << "this is battle sum " << i << endl;
		cout << "This is enemy HP " << enemy->getHp() << endl;
		enemy->damageTaken(i);
		cout << "remaining Health of the enemy is " << enemy->getHp() << endl << endl;
		
		system("pause");
		cout << endl;

		i = battleIncident(player->getVit(), enemy->getPow());
		cout << "Enemy dealt " << i << " to the Player " << endl;
		if (i < 0) (i * -1);
		else if (i == 0) i = 1;
		cout << "This is Your HP " << player->getHp() << endl;
		player->damageTaken(i);
		cout << "Your remaining Health is " << player->getHp() << endl;
	}
	else if (player->getAgi() < enemy->getAgi())
	{
		cout << "Enemy goes first" << endl << endl;
		i = battleIncident(rand() % player->getVit(), rand() % enemy->getPow());
		cout << "Enemy dealt " << i << " to the Player " << endl;
		if (i < 0) (i * -1);
		else if (i == 0) i = 1;
		cout << "This is Your HP " << player->getHp() << endl;
		player->damageTaken(i);
		cout << "Your remaining Health is " << player->getHp() << endl << endl;

		system("pause");
		cout << endl;

		i = battleIncident(rand() % enemy->getVit(), rand() % player->getPow());
		cout << "Player dealt " << i << " to the enemy " << endl;
		if (i < 0) (i * -1);
		else if (i == 0) i = 1;
		cout << "This is enemy HP " << enemy->getHp() << endl;
		enemy->damageTaken(i);
		cout << "remaining Health of the enemy is " << enemy->getHp() << endl << endl;

	}
}



int main()
{
	srand(time(NULL));
	string name;
	int choice;

	cout << "Select a Class: " << endl << endl;
	cout << "[1]  Warrior" << endl;
	cout << "[2]  Assassin" << endl;
	cout << "[3]  Mage" << endl << endl;

	cout << "Choice: ";
	cin >> choice;

	switch (choice)
	{
	default:
		cout << "No Class like that retry" << endl;
		break;
	case 1:
		cout << "You've chosen the Warrior class" << endl;
		break;
	case 2:
		cout << "You've chosen the Assassin class" << endl;
		break;
	case 3:
		cout << "You've chosen the Wizard class" << endl;
	}
	
	system("pause");
	system("cls");

	cout << "Enter Name: ";
	cin >> name;
	
	cout << "Ah!, so you've chosen the name " << name << " what a fine name for a player." << endl;

	system("pause");
	system("cls");

	character* Character = new character(name, rand() % (25 + 10) + 10, rand() % 10 + 5, rand() % 10 + 5, rand() % 10 + 5, rand() % 10 + 5);
	
	for (int i = 1; i != 0; i++)
	{
		character* Character2 = new character("Enemy", ((rand() % 25 + 10) + i), ((rand() % 10 + 5) + i), ((rand() % 10 + 5) + (i + 2)), ((rand() % 10 + 5) + (i + 1)), ((rand() % 10 + 5) + (i + 3)));

		cout << "=== Round: " << i << " ===" << endl;

		cout << Character->getName() << endl;
		switch (choice)
		{
		default:
			cout << "BLANK" << endl;
			break;
		case 1:
			cout << "Warrior" << endl;
			break;
		case 2:
			cout << "Assassin" << endl;
			break;
		case 3:
			cout << "Wizard" << endl;
		}
		cout << "==========" << endl;

		cout << "HP: " << Character->getHp() << endl;
		cout << "POW:  " << Character->getPow() << endl;
		cout << "VIT: " << Character->getVit() << endl;
		cout << "AGI: " << Character->getAgi() << endl;
		cout << "DEX: " << Character->getDex() << endl << endl;

		cout << "= = = VERSUS = = =" << endl << endl;

		cout << Character2->getName() << endl;
		randClass(choice);
		cout << endl << "HP: " << Character2->getHp() << endl;
		cout << "POW:  " << Character2->getPow() << endl;
		cout << "VIT: " << Character2->getVit() << endl;
		cout << "AGI: " << Character2->getAgi() << endl;
		cout << "DEX: " << Character2->getDex() << endl << endl;

		system("pause");
		system("cls");

		cout << "FIGHT: " << endl << endl;
		while (Character->getHp() >= 0 && Character2->getHp() >= 0)
		{
			battle(Character, Character2);
			  
			system("pause");
			if (Character2->getHp() <= 0)
			{
				system("cls");
				cout << "Player gets healed by: " << endl;
				Character->battleHeal(Character->getHp());
				delete Character2; Character2 = NULL;
				break;
			}
			else if (Character->getHp() <= 0)
			{
				break;
			}
		}

		if (Character->getHp() == 0)
		{
			delete Character; Character = NULL;
			break;
			i = 0;
		}

	}

	return 0;
}