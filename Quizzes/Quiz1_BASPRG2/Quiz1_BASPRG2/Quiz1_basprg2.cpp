#include <iostream>
#include <string>
#include <time.h>

using namespace std;

void packageSort(int packages[])
{
	for (int i = 0; i < 7; i++)
	{
		for (int n = 0; n < 7 - i - 1; n++)
		{
			if (packages[n] > packages[n+1])
			{
				swap(packages[n], packages[n+1]);
			}
		}
	}
}


int main()
{	
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int gold = 250;
	int itemPrice;
	string answer = "yes";

	packageSort(packages);

	

	while (answer == "yes"|| answer == "Yes" || answer == "YES")
	{
		cout << "Current Gold: " << gold << endl;
		cout << "State price of Item you wish to buy: ";
		cin >> itemPrice;

		if (itemPrice <= gold)
		{
			cout << "Item Sold! " << endl;
			gold = gold - itemPrice;
		}
		else if (itemPrice > gold)
		{
			cout << "current gold isn't enough" << endl << endl;

			for (int i = 0; i < 7; i++)
			{
				if (itemPrice <= packages[i])
				{
					cout << "Package " << i + 1 << ": ";
					cout << packages[i] << endl;
					cout << "Would the user like to buy the package?";
					cin >> answer;

					if (answer == "yes" || answer == "Yes" || answer == "YES" || answer == "Y" || answer == "y")
					{
						gold = gold + packages[i];

						cout << "Package Bought! " << endl;
						cout << "Your current gold is now: " << gold << endl;

						gold = gold - itemPrice;
						cout << "Item Bought! " << endl;
						cout << "Your gold is now: " << gold << endl;

						break;
					}
					else if (answer == "No" || answer == "NO" || answer == "no" || answer == "N" || answer == "n")
					{
						cout << "uh.. ok" << endl;
						break;
					}
				}
				else if (itemPrice > 250000)
				{
					cout << "No package available" << endl;
					break;
				}

			}
		}

		cout << "Would the user like to continue shopping?[Yes / No]: ";
		cin >> answer;

		if (answer == "No" || answer == "NO" || answer == "no")
		{
			cout << "Thank you! Come again!" << endl;
			break;
		}
		

		system("pause");
		system("cls");
	}
	return 0;
}